#!/usr/bin/gnuplot

# (c) L. Apvrille - 2015

# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


set style line 2 lt 1 lw 3 lc rgb "#ff0000"
set style line 3 lt 1 lw 3 lc rgb "#00ff00"
set style line 4 lt 1 lw 3 lc rgb "#0000ff"
set style line 5 lt 1 lw 3 lc rgb "#cc00cc"


set terminal png size 1200,800
set xdata time
set timefmt "%m:%d:%H:%M:%S"
set output "/usr/share/vmc/curve/vmc.png"
set autoscale x
set format x "%H"
set xtics 7200 # tic toutes les heures
#set autoscale y
set yrange [0:2.5]
set y2range [0:100] 
set y2tics 10
set grid
set xlabel "Date and time"
set ylabel "Speed"
set y2label "Temperature and humidity"
set title "VMC"
set key outside right box
plot "/usr/share/vmc/curve/times_vmc_generated.data" using 1:2 index 0 title "speed" with lines ls 2, '' using 1:3 axes x1y2  smooth bezier with lines title "Outside temp" ls 3, '' using 1:4 axes x1y2  smooth bezier with lines title "Inside temp" ls 4, '' using 1:5 axes x1y2 smooth bezier with lines title "Outside humidity" ls 5




