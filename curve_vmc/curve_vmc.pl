#!/usr/bin/perl

# (c) A. Apvrille - 2015

# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


use strict;
use warnings;
use Getopt::Long;
use Hijk;
use POSIX qw(strftime);

# ---------------------------
my $weewx_dir = '/home/weewx'; # Modify this to match your install of weewx

# url of CMV webserver
my $relayserver_url = 'PUT IP ADDRESS OF RPI1'; # CUSTOMIZE !!!
my $relayserver_port = 1974;
my $status_url = '/status';

# prints debug message when set to 1
my $debug = 0;

# ---------------------------

sub debug_log {
    my $message = shift;

    if ($debug) {
	print $message;
    }
}


# reads data from the weather station and returns
# inner temperature, outer temperature and outer humidity
sub read_weather {
    # This retrieves the most recent entry in the database
    my $dateTime=`sqlite3 $weewx_dir/archive/weewx.sdb "select max(dateTime) from archive;"`;
    chomp($dateTime);

    # make sure dateTime is sound
    my $currentTime = time();
    if ($currentTime - $dateTime > 10 * 60 * 60) {
	debug_log("Weewx time is obsolete");
    }


    # This gets the temperature for that entry
    my $inTemp=`sqlite3 /home/weewx/archive/weewx.sdb "select inTemp from archive where dateTime=${dateTime};"`;

    my $outTemp = `sqlite3 /home/weewx/archive/weewx.sdb "select outTemp from archive where dateTime=${dateTime};"`;

    my $outHumidity = `sqlite3 /home/weewx/archive/weewx.sdb "select outHumidity from archive where dateTime=${dateTime};"`;
    chomp($inTemp);
    chomp($outTemp);
    chomp($outHumidity);

    if ($inTemp < -20 || $inTemp > 50) {
	debug_log("Something wrong with inner temperature: $inTemp");
	$inTemp = '?';
    }

    if ($outTemp < -20 || $outTemp > 50) {
	debug_log("Something wrong with outer temperature: $outTemp");
	$outTemp = '?';
    }

    if ($outHumidity < 0 || $outHumidity > 100) {
	debug_log("Something wrong with outer humidity: $outHumidity");
	$outHumidity = '?';
    }
    

    debug_log("Inner temperature: $inTemp\n");
    debug_log("Outer temperature: $outTemp\n");
    debug_log("Outer humidity:    $outHumidity\n");
    return $inTemp, $outTemp, $outHumidity;
}

sub read_cmv_reply {
    my $page = shift;
    debug_log("--> read_cmv_reply()\n");
    my $speed;

    if ($page eq '?') {
	return '?';
    }

    if ($page =~ /Relay #1: ON/i) {
	if ($page =~ /Relay #2: ON/i) {
	    $speed = 2;
	} else {
	    $speed = 1;
	}
    } else {
	$speed = 0;
    }

    debug_log("<-- read_cmv_replay(): speed=$speed\n");
    return $speed;
}

sub get_page {
    my $path = shift;
    debug_log("--> get_page(): getting http://$relayserver_url:$relayserver_port$path\n");
    my $res;
    eval { # exception catcher

	$res = Hijk::request({
	    method       => "GET",
	    host         => "$relayserver_url",
	    port         => "$relayserver_port",
	    path         => "$path"});
    };
    if (! defined $res) {
	debug_log("Error: http://$relayserver_url:$relayserver_port/$path not responding...");
	return '?';
    }
    if (exists $res->{error} and $res->{error} ) {
	debug_log("Error: http://$relayserver_url:$relayserver_port/$path not responding...");
	return '?';
    } 
    debug_log("<-- get_page(): $res->{body}\n");
    return $res->{body};
}


# gets the current status and returns relay 1 and relay 2
sub get_status {
    debug_log("--> get_status(): Getting http://$relayserver_url:$relayserver_port$status_url\n");
    my $page = get_page( $status_url );
    return read_cmv_reply( $page );
}

sub get_time {
    my $current_time = strftime("%m:%d:%H:%M:%S", localtime);
    debug_log("get_time(): $current_time\n");
    return $current_time;
}

sub usage {
    print "curve_vmc.pl [--verbose]\n";
    print "--verbose blah blah\n";
    die("Quitting");
}


# --------- MAIN ----------------------------------------
my $output_file;

GetOptions ("verbose" => \$debug
     )
    or usage();

# read current weather data
my ($intemp, $outtemp, $outhumidity) = read_weather();

# read VMC speed
my $speed = get_status();

# read time
my $mytime = get_time();

print "$mytime $speed $outtemp $intemp $outhumidity\n";

exit 1;
