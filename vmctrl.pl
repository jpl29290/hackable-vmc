#!/usr/bin/perl

# CMV Controlled Mechanical Ventilation
# (c) A. Apvrille - 2015

# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
#use LWP::Simple; too slow
use Hijk;
use Getopt::Long;
use Sys::Syslog qw(:standard :macros);

# ------------ CONFIGURATION ------------------------
# url of CMV webserver
my $relayserver_url = 'PUT IP ADDRESS OF RPI1';
my $relayserver_port = 1974;

# you usually need not modify this
my $status_url = '/status';
my $ofrelay_url = '/ofRelay';
my $onrelay_url = '/onRelay';

# duration of pulse
my $sleep_time = 60 * 30;

# the format of both files is the same:
# time,relay1,relay2
#
# file which stores the status of the CMV before pulse
# this file must be in rw mode
my $archive_file = '/var/tmp/vmc_pulse.archive';

# prints debug message when set to 1
my $debug = 0;
# ---------------------------------------------------

sub debug_log {
    my $message = shift;
    my $current = time();

    if ($debug) {
	print "[$current] $message";
    }
}

sub write_file {
    my $filename = shift;
    my $r1 = shift;
    my $r2 = shift;

    debug_log("--> write_file(): Writing $filename\n");

    my $current_time = time();
    open(my $fh, ">$filename") or die "Error: Cannot open file for writing: $filename";
    print $fh "$current_time,$r1,$r2";
    close($fh);

    debug_log("Timer: $current_time\n");
    debug_log("Previous value of relay1: $r1\n");
    debug_log("Previous value of relay2: $r2\n");
    debug_log("<-- write_file()\n");
    syslog(LOG_DEBUG, "Updating $filename: time=$current_time, relay1=$r1, relay2=$r2");
}


sub read_cmv_reply {
    my $page = shift;
    debug_log("--> read_cmv_reply()\n");
    my $r1;
    my $r2;

    if ($page =~ /Relay #1: ON/i) {
	$r1 = 1;
    } else {
	$r1 = 0;
    }

    if ($page =~ /Relay #2: ON/i) {
	$r2 = 1;
    } else {
	$r2 = 0;
    }

    debug_log("  Relay1: $r1\n");
    debug_log("  Relay2: $r2\n");
    debug_log("<-- read_cmv_replay()\n");
    return ($r1, $r2);
}

sub get_page {
    my $path = shift;
    debug_log("--> get_page(): getting http://$relayserver_url:$relayserver_port$path\n");
    my $res;
    eval { # exception catcher

	$res = Hijk::request({
	    method       => "GET",
	    host         => "$relayserver_url",
	    port         => "$relayserver_port",
	    path         => "$path"});
    };
    if (! defined $res) {
	syslog(LOG_INFO, "Error: http://$relayserver_url:$relayserver_port/$path not responding...");
	die $@;
    }
    if (exists $res->{error} and $res->{error} ) {
	syslog(LOG_INFO, "Error: http://$relayserver_url:$relayserver_port/$path not responding...");
	die "Couldn't get page: $path (error: $res->{error})";
    } 
    debug_log("<-- get_page(): $res->{body}\n");
    return $res->{body};
}

# gets the current status and returns relay 1 and relay 2
sub get_status {
    debug_log("--> get_status(): Getting http://$relayserver_url:$relayserver_port$status_url\n");
    syslog(LOG_DEBUG, "Getting status" );
    my $page = get_page( $status_url );
    return read_cmv_reply( $page );
}

# sets a given relay to a given value
# arg1 is relay number: 1 or 2
# arg2 is its value: 0 or 1
sub set_relay {
    my $relay_number = shift;
    my $value = shift;

    debug_log("--> set_relay(): Asking to set relay $relay_number to $value...\n");

    # checking arguments
    ($relay_number == 1 || $relay_number == 2) or die "Error: Bad relay number value: $relay_number";
    ($value == 0 || $value == 1) or die "Error: Bad relay value: $value";

    # building url
    my $url = $onrelay_url;

    if ($value == 0) {
	$url = $ofrelay_url;
    }

    if ($relay_number == 1) {
	$url .= '1';
    } else {
	$url .= '2';
    }

    # request
    debug_log("<-- set_relay(): Getting $url\n");
    syslog(LOG_INFO, "Setting relay $relay_number to $value: getting $url");
    my $page = get_page( $url );
    return read_cmv_reply( $page );
}


# sets the CMV to a given speed
# returns the current status
sub set_speed {
    my $speed = shift;
    my $current_relay1 = shift;
    my $current_relay2 = shift;

    debug_log("--> set_speed(): Requesting speed $speed...\n");
    debug_log("  Current relay1: $current_relay1, current relay2: $current_relay2\n");

    ($speed == 1 || $speed == 2) or die "Error: Bad speed value: $speed";
    ($current_relay1 == 0 || $current_relay1 == 1) or die "Error: Bad relay1 value: $current_relay1";
    ($current_relay2 == 0 || $current_relay2 == 1) or die "Error: Bad relay2 value: $current_relay2";

    if ($current_relay1 != 1) {
	debug_log("  Switching on CMV.\n");
	($current_relay1, $current_relay2) = set_relay(1, 1);
    }

    if ($current_relay2 != ($speed - 1)) {
	debug_log("  Setting speed\n");
	($current_relay1, $current_relay2) = set_relay(2, $speed - 1);
    }
    return ($current_relay1, $current_relay2);
}

# reads the stored timer and relay1 and relay2 values
sub read_archive_file {
    my $filename = shift;
    (-e $filename) or die "Error: Archive file $filename does not exist";

    debug_log("--> read_archive_file(): Reading archive $filename\n");
    open(my $fh, "$filename") or die "Error: Cannot open archive file: $filename";
    my $line = <$fh>;
    close($fh);
    
    chomp $line;
    my ($timer, $archive1, $archive2) = split(/,/, $line);
    debug_log("  Timer: $timer\n");
    debug_log("  Previous value of relay1: $archive1\n");
    debug_log("  Previous value of relay2: $archive2\n");
    debug_log("<-- read_archive_file()\n");
    return ($timer, $archive1, $archive2);
}


sub destroy_archive {
    if (-e $archive_file) {
	debug_log("destroy_archive(): Removing $archive_file\n");
	# remove the archive file
	unlink($archive_file);
    }
}

sub check_old_archive {
    debug_log("check_old_archive(): Checking if there's an old archive\n");
    if (-e $archive_file) {
	my ($archive_timer, $archive1, $archive2) = read_archive_file($archive_file);
	my $current_time = time();
	if ($archive_timer + $sleep_time < $current_time) {
	    # timer in the archive has elapsed
	    debug_log("  Timer has elapsed\n");
	    destroy_archive();
	}
    }
}


sub revert_pulse {
    debug_log("revert_pulse(): Asking to reverse pulse...\n");
    (-e $archive_file) or die "Error: no archive file to revert $archive_file";

    my ($archive_timer, $archive1, $archive2) = read_archive_file($archive_file);
    my $r1;
    my $r2;

    # we first set relay 2 so as to eventually put it to low
    ($r1, $r2) = set_relay(2, $archive2);

    if ($r1 != $archive1) {
	sleep(1);
	($r1, $r2) = set_relay(1, $archive1);
    }

    return ($r1, $r2);
}

# cancel an ongoing pulse
sub cancel_pulse {
    debug_log("--> cancel_pulse(): Asking to cancel pulse...\n");
    my $r1;
    my $r2;
    if (-e $archive_file) {
	($r1, $r2) = revert_pulse();
	destroy_archive();
    } else {
	debug_log("  there's no ongoing pulse, so we can't cancel it!\n");
	($r1, $r2) = get_status();
    }
    return ($r1, $r2);
}


sub pulse {
    my $speed = shift;

    debug_log("pulse(): Requesting Pulse Speed $speed...\n");

    check_old_archive();
    my ($r1, $r2) = get_status();

    if (! -e $archive_file) {
	# there's no archive file currently
	write_file($archive_file, $r1, $r2);

	# set CMV to the appropriate speed
	set_speed( $speed, $r1, $r2 );

	debug_log("  Sleeping for $sleep_time seconds...\n");
	sleep( $sleep_time );

	# during sleep, someone might have erased the archive
	if (-e $archive_file) {
	    # revert back to previous status
	    ($r1, $r2) = revert_pulse();
	    destroy_archive();
	}
    } else {
        # else there's a current ongoing pulse 
	debug_log("There's an ongoing pulse. Sorry!\n");
	
    }
    return ($r1, $r2);
}

sub usage {
    print "./vmctrl.pl [--pulse=speed] [--cancel]\n";
    print "           [--stop] [--low] [--high]\n";
    print "           [--verbose] [--timer=seconds]\n";
    print "\nThis program controls the Controlled Mechanical Ventilation (CMV)";
    print "\nIt directly communicates with the Relayserver next to the VMC and does not care";
    print "\nabout web or automatic modes.";
    print "\nThe program is unable to operate if the Relayserver's server is down.\n\n";
    print "\t--pulse=speed\tSets pulse to speed 1 or 2\n";
    print "\t--cancel\tCancels an ongoing pulse\n";
    print "\t--stop\tStops the CMV altogether\n";
    print "\t--status\tGets the current status\n";
    print "\t--low\tStarts CMV to speed 1\n";
    print "\t--high\tStarts CMV to speed 2\n";
    print "\t--verbose\tVerbose messaging\n";
    print "\t--timer=seconds\tSets the pulse timer to X seconds\n";
    print "\nCurrent pulse timer is $sleep_time seconds\n";
    exit(1);
}

# MAIN -----------------------
my $pulse_speed;
my $cancel;
my $stop;
my $low;
my $high;
my $status;
my $relay1;
my $relay2;

GetOptions ("pulse=s" => \$pulse_speed,
	    "timer=s" => \$sleep_time,
	    "cancel" => \$cancel,
	    "stop" => \$stop,
	    "status" => \$status,
	    "low" => \$low,
	    "high" => \$high,
	    "verbose" => \$debug)
    or usage();

openlog('vmctrl', 'pid', LOG_USER);

if (defined $pulse_speed) {
    syslog(LOG_INFO, "Requesting a pulse $pulse_speed");
    ($relay1, $relay2) = pulse( $pulse_speed );
}

if (defined $cancel) {
    syslog(LOG_INFO, "Requesting to cancel pulse");
    ($relay1, $relay2) = cancel_pulse();
}

if (defined $status) {
    syslog(LOG_INFO, "Requesting status");
    ($relay1, $relay2) = get_status();
}

if (defined $stop) {
    syslog(LOG_INFO, "Requesting to stop");
    destroy_archive();

    # we want to stop in speed 1
    ($relay1, $relay2) = set_relay(2, 0);

    sleep(1);

    # stop the CMV
    if ($relay1 != 0) {
	($relay1, $relay2) = set_relay(1, 0);
    }
}

if (defined $low) {
    syslog(LOG_INFO, "Requesting speed 1");
    destroy_archive();

    # set low speed
    ($relay1, $relay2) = set_relay(2, 0);

    # if necessary start the CMV
    if ($relay1 != 1) {
	($relay1, $relay2) = set_relay(1, 1);
    }
}

if (defined $high) {
    syslog(LOG_INFO, "Requesting speed 2");
    destroy_archive();

    # if necessary start the CMV
    ($relay1, $relay2) = set_relay(1, 1);

    if ($relay2 != 1) {
	# set high speed
	($relay1, $relay2) = set_relay(2, 1);
    }
}
closelog();

if (defined $relay1 && defined $relay2) {
    print STDOUT "$relay1,$relay2\n";
}

exit 0;


