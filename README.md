Requirements
=============

Hardware:
- two Raspberry Pi, running Raspbian and on the same LAN
- a weather station that works with Weewx


Software:
- Weewx (http://weewx.com/)
- Gnuplot
- A web server
- Python 2.7


License
=========
Our scripts are released under GPL (https://www.gnu.org/licenses/gpl-3.0.fr.html)


Install
=========

This is not packaged at all, and you'll probably have to customize it to your needs.


* On RPI1: 

  Copy webserver4relays_4_fruitic.py to /usr/share/vmc (for example)

  Then, in /etc/rc.local, make sure to add:

  ```
  for i in 17 22 23 27
  do 
     echo $i > /sys/class/gpio/export
     echo out > /sys/class/gpio/gpio$i/direction
  done

  /usr/bin/python /usr/share/vmc/webserver4relays_4_fruitic.py &
  ```

  This ensures 1/ that the GPIOs are usable and 2/ that the basic web server is running



* On RPI2:

  Install Weewx (sudo apt-get install weewx). Make sure this works before you go to the next step.
  If you do not install weewx in /home/weewx, you'll probably have to customize a few directories
  in our scripts (grep weewx for them) but that should work fine.

  Create a directory /usr/share/vmc for example. We refer to this directory as vmc_dir.

  Create a directory /var/tmp. This directory is used to store some status files. On my RPi, this
  is a RAM fs.

  Copy those files to /usr/share/vmc :

  - autovmc.config : 
               modify the thresholds in that file to your needs

  - vmctrl.pl :
    	       relayserver_url : set IP address of RPI1
	       usually you need not modify other parameters

  - autovmc.pl :
    	       weewx_dir : root directory of Weewx (weather station software)
	       vmc_dir : specify the right directory

  - curve_vmc/curve_vmc.pl :
    		weewx_dir : root directory of Weewx (weather station software)	  
                relayserver_url : set IP address of RPI1

  - curve_vmc/curve_script : 
                to adapt to your own needs

  - curve_vmc/plot_vmc.gp : that's the gnuplot script

  Make sure Bash and Perl scripts are executable :

  ```
  chmod u+x vmctrl.pl autovmc.pl curve_vmc.pl curve_script
  ```

  Add the desired tasks to your crontab : 
  
  ```
  $ crontab -e
  */10 7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 * * * /usr/share/vmc/autovmc.pl &> /dev/null
  0 22 * * * /usr/share/vmc/vmctrl.pl --stop &> /dev/null
  */10 * * * * /usr/share/vmc/curve/curve_script &> /dev/null
  ```