# Copyright Jon Berg , turtlemeat.com http://fragments.turtlemeat.com/pythonwebserver.php
# Modified by L. Apvrille, 2015

import RPi.GPIO as GPIO
import syslog

from math import *
import string,cgi,time
from os import curdir, sep
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
#import pri

import time
# at the beginning of the script
startTime = time.time()
# ...
def getUptime():
    """
    Returns the number of seconds since the program started.
    """
    # do return startTime if you just want the process start time
    return time.time() - startTime

off = "OFF"
on = "ON"
nbOfRequests = 1
relay1String = off
relay2String = off
relay1state = 0
relay2state = 0
relay1GPIO = 22
relay2GPIO = 23
PORT = 1974


def myPrint(s):
    s1 = "[WSR] %s" %(s)
    syslog.syslog(s1)


class MyHandler(BaseHTTPRequestHandler):

    def do_GET(s):
           """Respond to a GET request."""
           myPrint("handling request")
           global nbOfRequests
           global relay1String
           global relay2String
           up = getUptime()
           msg = "Handling request at %d" %(up)
           myPrint(msg)
           if (up != 0):
               up = ceil(up*100)/100
           uptime_string = str(up) + " sec."
           s.send_response(200)
           s.send_header("Content-type", "text/html")
           s.end_headers()


           #Testing s
           if (s.path[1:] == "onRelay1"):
               onRelay1()
           if (s.path[1:] == "ofRelay1"):
               offRelay1()
           if (s.path[1:] == "onRelay2"):
               onRelay2()
           if (s.path[1:] == "ofRelay2"):
               offRelay2()

           myPrint("Preparing answer")
           s.wfile.write("<html><head><title>VMC SERVER</title></head>")
           #s.wfile.write("<body><p>This is a test.</p>")
           # If someone went to "http://something.somewhere.net/foo/bar/",
           # then s.path equals "/foo/bar/".
           req = "<p>Nb Of requests:" + str(nbOfRequests) + "</p>"
           s.wfile.write("<p>You accessed path: %s</p>" % s.path)
           s.wfile.write("<p>Uptime: %s</p>" % uptime_string)
           s.wfile.write(req)
           myPrint("Preparing relay answer")
           s.wfile.write("<p>Relay #1: %s</p>" % relay1String)
           s.wfile.write("<p>Relay #2: %s</p>" % relay2String)
           nbOfRequests +=1
           myPrint("Request: All done")

def onRelay1():
    global relay1String
    myPrint("on Relay 1")
    if(GPIO.input(relay1GPIO) == GPIO.HIGH):
        myPrint("Applying Relay 1 order (on)")
        relay1String = on
        GPIO.output(relay1GPIO, GPIO.LOW)
    myPrint("Relay 1 on done")
         
def offRelay1():
    global relay1String
    myPrint("off Relay 1")
    if(GPIO.input(relay1GPIO) == GPIO.LOW):
        myPrint("Applying Relay 1 order (off)")
        relay1String = off
        GPIO.output(relay1GPIO, GPIO.HIGH)
    myPrint("Relay 1 off done")

def onRelay2():
    global relay2String
    myPrint("on Relay 2")
    if(GPIO.input(relay2GPIO) == GPIO.HIGH):
        myPrint("Applying Relay 2 order (on)")
        relay2String = on
        GPIO.output(relay2GPIO, GPIO.LOW)
    myPrint("Relay 2 on done")

def offRelay2():
    global relay2String
    myPrint("off Relay 2")
    if(GPIO.input(relay2GPIO) == GPIO.LOW):
        myPrint("Applying Relay 2 order (off)")
        relay2String = off
        GPIO.output(relay2GPIO, GPIO.HIGH)
    myPrint("Relay 2 off done")


def main():
    try:
        # configuring IOs
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(relay1GPIO,GPIO.OUT)
        GPIO.setup(relay2GPIO,GPIO.OUT)

        offRelay1()
        offRelay2()
        #Managing webserver
        myPrint('Starting relay server')
        server = HTTPServer(('', PORT), MyHandler)
        myPrint('Started httpserver...')
        server.serve_forever()
    except KeyboardInterrupt:
        myPrint('^C received, shutting down server')
        server.socket.close()

if __name__ == '__main__':
    main()
